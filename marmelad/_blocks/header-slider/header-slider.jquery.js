$('._js_header-slider').owlCarousel({
    items: 1,
    autoplay: true,
    loop: true,
    margin: 0,
    nav: false,
    mouseDrag: false,
    animateOut: 'fadeOut',
    animateIn: 'slideOutin'
})