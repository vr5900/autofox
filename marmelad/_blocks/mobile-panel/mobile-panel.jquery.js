var menuFlag = 1;
var sliderFlag = 1;

$('.mobile-panel__m-burger').on('click tap', function(){
    $('._js_mobile-panel').toggleClass('opened');
})

$(window).on('resize load', function() {
    setTimeout(function(){
        $('._js_header-slider').css({
            'margin-top': -$('._js_header-height').innerHeight(),    
            'height': $('.app-header').height()
        });
    }, 500);

    if($(window).width() <= 768 && menuFlag == 1) {
        $('.app-header__menu').appendTo('._js_mobile-panel');
        $('.app-header__phone').appendTo('._js_mobile-panel');
        menuFlag = 2;
    } else if ($(window).width() > 768 && menuFlag == 2) {
        $('.app-header__menu').appendTo('.app-header__content');
        $('.app-header__phone').appendTo('.app-header__content');
        menuFlag = 1;
    };

    if($(window).width() <= 500 && sliderFlag == 1) {
        $('.header-main-block__slider-item').css({
            'background-position': '-100% 110%'
        });
        sliderFlag = 2;
    } else if ($(window).width() > 500 && sliderFlag == 2) {
        $('.header-main-block__slider-item').css({
            'background-position': 'right top'
        });
        sliderFlag = 1;
    }
});