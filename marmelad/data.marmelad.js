module.exports = {
    app      : {
        lang      : 'ru',
        stylus    : {
            theme_color : '#ff9803'
        },
        GA        : false, // Google Analytics site's ID
        package   : 'ключ перезаписывается значениями из package.json marmelad-сборщика',
        settings  : 'ключ перезаписывается значениями из файла настроек settings.marmelad.js',
        storage   : 'ключ перезаписывается значениями из файла настроек settings.marmelad.js',
        buildTime : '',
        controls : [
            'default',
            'brand',
            'success',
            'info',
            'warning',
            'danger'
        ]
    },

    pageTitle: 'marmelad',
    menuTop : [
        {
            'name': "Каталог"
        },
        {
            'name': "о нас",
            'subLevelClass': true
        },
        {
            'name': "контакты",
            'activeClass': true
        }
    ],

    imageBlock : [
        {
            'pic': 'img/test-img.png',
            'title': 'Lorem ipsum',
            'desc': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
        },
        {
            'pic': 'img/test-img.png',
            'title': 'Lorem ipsum',
            'desc': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
        },
        {
            'pic': 'img/test-img.png',
            'title': 'Lorem ipsum',
            'desc': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
        }
    ],
    form : {
        name : "Название формы",
        mod : "",
        items: [
            {
                'name': "Имя",
                'type': 'text',
                'note': 'Это поле обязательно для заполнения',
                'mod': '',
                'placeholder': true,
                'required': true,
                'error': false,
            },
            {
                'name': "Сообщение",
                'type': 'textarea',
                'note': '',
                'mod': '',
                'placeholder': true,
                'required': false,
                'error': false,
            },
            {
                'name': "Выпадающий список",
                'type': 'select',
                'note': '',
                'mod': '',
                'placeholder': true,
                'required': false,
                'error': false,
                options : [
                    {
                        'name' : 'value 1'
                    },
                    {
                        'name' : 'value 2'
                    },
                    {
                        'name' : 'value 3'
                    },
                    {
                        'name' : 'value 4'
                    },
                ]
            },
            {
                'name': "Текстовый блок",
                'type': 'textBlock',               
                'text': 'Lorem ipsum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',    
                'placeholder': true,
            },
            {
                'name': "Радиобокс",
                'type': 'radio',
                'note': '',
                'mod': '',
                'placeholder': true,
                'required': false,
                'error': false,
            },
            {
                'name': "Имя",
                'type': 'checkbox',
                'note': '',
                'mod': '',
                'placeholder': true,
                'required': true,
                'error': false,
            }
        ]
    }

};
